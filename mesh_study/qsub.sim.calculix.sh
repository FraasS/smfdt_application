#!/bin/sh

cd $PBS_O_WORKDIR

cat $PBS_JOBID > jobid

cat $PBS_NODEFILE
cat $PBS_NODEFILE > nodes

lsOutput=$(ls)
solverFile=($(echo "$lsOutput" find | grep final))

export OMP_NUM_THREADS=100

ccx ${solverFile:0:-4} > log_ca 2>&1

touch simulation.finished
