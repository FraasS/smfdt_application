#!/bin/sh

cd $PBS_O_WORKDIR

cat $PBS_JOBID > jobid

cat $PBS_NODEFILE
cat $PBS_NODEFILE > nodes

#export OMP_NUM_THREADS=100
lsOutput=$(ls)
interpolationFile=($(echo "$lsOutput" find | grep fbd))

cgx -bg $interpolationFile > log.ca.interpolating 2>&1

touch interpolation.finished
