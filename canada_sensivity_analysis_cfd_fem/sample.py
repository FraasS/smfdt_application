from SALib.sample import morris
import os
import sys
import numpy as np

# Define the model inputs
problem = {
    'num_vars':2,
    'names': ['cV_ru_maxThickness_a_0', 'cV_ru_maxThickness_a_1'],
    'bounds': [[0.37, 0.43],
              [0.06, 0.12]]
}

# Generate samples
param_values = morris.sample(problem,10,4)
fileName = os.path.join(sys.argv[1], "param_values.txt")
header=str(problem['names'])[1:-1] 
np.savetxt(fileName, param_values, header=header.replace("'",""))
