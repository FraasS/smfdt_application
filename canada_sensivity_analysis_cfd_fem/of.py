import numpy as np
import sys
import os

sys.path.append('/home/st/st_us-042020/st_ac132161/sandbox/dtOO/scripts/python')
from pyDtOO import *

omega = 9.42478  
              
path = os.path.join(sys.argv[1], "results_of.txt")

if sys.argv[len(sys.argv)-1] == "postProcessingWriteData":  
       
  Q_ru_dev = dtScalarDeveloping( dtDeveloping(
    os.path.join(sys.argv[2], sys.argv[3] + "_" + sys.argv[4], 'postProcessing/swakExpression_Q_ru_in')
    ).Read() )
  pIn_ru_dev = dtScalarDeveloping( dtDeveloping(
    os.path.join(sys.argv[2], sys.argv[3] + "_" + sys.argv[4], 'postProcessing/swakExpression_ptot_ru_in')
    ).Read() )
  pOut_ru_dev = dtScalarDeveloping( dtDeveloping(
    os.path.join(sys.argv[2], sys.argv[3] + "_" + sys.argv[4], 'postProcessing/swakExpression_ptot_ru_out')
    ).Read() )
  Vcav = dtScalarDeveloping( dtDeveloping( 
    os.path.join(sys.argv[2], sys.argv[3] + "_" + sys.argv[4], 'postProcessing/swakExpression_V_CAV') 
    ).Read() )     
  F_dev = dtForceDeveloping( dtDeveloping(
    os.path.join(sys.argv[2], sys.argv[3] + "_" + sys.argv[4], 'postProcessing/forces')
    ).Read({'force.dat' : ':,4:10', 'moment.dat' : ':,4:10', '*.*' : ''}) )
  #
  # power
  #
  M = F_dev.MomentMeanLast(100)[2]
  P = F_dev.MomentMeanLast(100)[2] * omega
  
  #
  # head
  #
  
  dHRunner = (pOut_ru_dev.MeanLast(100) - pIn_ru_dev.MeanLast(100)) / 9.81
    
  #
  # eta
  #
  
  eta = P / ( 1000. * 9.81 * dHRunner * Q_ru_dev.MeanLast(100) )
   
  f = open(path, "a+")
  f.write(sys.argv[4])
  f.write(", ")
  f.write(str(P))
  f.write(", ")
  f.write(str(dHRunner))
  f.write(", ")
  f.write(str(eta))
  f.write(", ")
  f.write(str(Vcav.MeanLast(100)))
  f.write("\n")
  f.close()
  
elif sys.argv[len(sys.argv)-1] == "postProcessingSortData":    
  
  # sort
  resultData = np.genfromtxt(path, delimiter = ',', dtype = str)
  sampleNames = np.zeros(np.size(resultData[:,0]))
  for i in range(np.size(resultData[:,0])):
    sampleNames[i] = int(resultData[i,0].replace(os.path.join(sys.argv[2] + "_"), ''))
  indexList = sorted(range(len(sampleNames)), key=lambda k: sampleNames[k])
  resultDataCopy = resultData.copy()
  for i in range(np.size(resultData[:,0])):
      resultData[i] = resultDataCopy[indexList[i]]
  f = open(path, "w")
  f.write("# Case, Power, dHRunner, Eta, Vcav")
  f.write("\n")
  np.savetxt(f, resultData, fmt="%s, %s, %s, %s, %s", comments='', delimiter=',')
  f.close()  
  
