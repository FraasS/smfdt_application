import sys
sys.path.append("/home/stefanf/sandbox/smfdt/scripts")
from python import *
import os
import numpy as np

#
# Define the variables for calculix
#

#
# variables for interpolating
#

pathFoamCase = sys.argv[1]
timeStep = '100'
patchNameFoam = ['RU_BLADE']
referencePressure = 100
patchNameCalculix = ['RUBLADE']

#
# variables for writing solver file
#

pathCalculixCase = sys.argv[2]
solverFile = sys.argv[3]+'_ruWithRounding_mechMesh_2'
elementName = 'Volume1'
boundaryName = ['RU_HUB_FIX']
setName = ['pressure']
force = False
setNameForce = ['none']
omega = 9.42477796
rotationAxis = [0,0,0,0,0,0.1]
eModul = '210000.0E6'
density = '7.8E3'
poissonCoefficient = '.3'
solver = 'SPOOLES'

#
# Interpolation
#

if sys.argv[len(sys.argv)-1] == "interpolateValues":
    print (pathCalculixCase)
    print (solverFile)
    #
    # correct the mesh file for interpolation
    #
    
    writeSolverFile(solverFile, pathCalculixCase).WriteSolverInterpolateFile()
    
    #
    # correct the pressure field for interpolation
    #
    
    writeShearStressAndPressure(pathFoamCase, timeStep, patchNameFoam, referencePressure).WritePressure()
    
    #
    # write file for interpolation
    #

    f = open(os.path.join(pathCalculixCase, 'valueInterpolation.fbd'), 'w')
    f.write("read ")
    f.write(pathFoamCase)
    f.write(" foam")
    f.write("\nread ")
    f.write(solverFile+'_interpolate.inp')
    f.write(" add")
    f.write("\nseto master\n")
    for i in range(np.size(patchNameFoam)):
        f.write("seta master f ")
        f.write(patchNameFoam[i])
        f.write("\n")
    f.write("comp master do\n")
    for i in range(np.size(patchNameCalculix)):
        f.write("seta pressure n ")
        f.write(patchNameCalculix[i])
        f.write("\n")
    f.write("comp pressure do\n")
    f.write("map pressure master surf ds1\nsend pressure abq pres ds1 e1")
    f.write("\nsend ")
    f.write(elementName)
    f.write(" abq")
    f.write("\n\nquit")
    f.close()

#
# Write the solver file
#
    
elif sys.argv[len(sys.argv)-1] == "writeSolverFile":
    
    writeSolverFile(
            solverFile, pathCalculixCase, elementName, boundaryName, setName, force,
            setNameForce, omega, rotationAxis, eModul, density, poissonCoefficient, solver
            ).WriteSolverFileGmsh()
    


    