#!/bin/sh

#
# go to submit directory
#
#cd $SLURM_SUBMIT_DIR

#module load cae/openfoam/v2206
#source $FOAM_INIT

#export FOAM_SIGFPE=false
source /home/stefanf/OpenFOAM/OpenFOAM-v2112/openfoam/etc/bashrc


_caseName=$1 #tistos_ru_of
_state=$2 #T1_1

#if ls $pathToMeshGeneration | grep -q $4"_"$1"_"*.*2.inp; then

#	echo "mesh for case" $4"_"$1 "allready exits, skipping mesh generation"
#	exit
#else
python3 createStates.py $_caseName $_state
#fi
