#!/bin/bash

_mainDir=$1 #/mnt/scratch/procan/Optimierer/AutoOpt04_Shroud
_caseName=$2 #tistos_ru_of
_state=$3 #T1_1
_pidDir=$4
_pidFile="${_pidDir}/${0}.${$}.pid"

echo "Check for ${_pidDir} directory"
if [ ! -d "${_pidDir}" ]; then
  echo "Create ${_pidDir} directory"
  mkdir ${_pidDir}
fi

echo "Touch _pidFile = ${_pidFile}"
touch ${_pidFile}
echo "${_mainDir}" >> ${_pidFile}
echo "${_caseName}" >> ${_pidFile}
echo "${_state}" >> ${_pidFile}
echo "${_caseName} : ${_state} : start"

#
# submit mesh script
#

sh sbatch.sg.sh $_caseName $_state
sh sbatch.ng.sh $_caseName $_state

source /home/stefanf/OpenFOAM/OpenFOAM-v2112/openfoam/etc/bashrc


for i in "_tl_" "_n_" "_vl_";do
    foamCaseDir=$_mainDir/$_caseName$i$_state
    calculixCaseDir=$foamCaseDir"/calculix"
    
    #
    # run openFoam
    #
    cd $_mainDir
    
    cp sbatch.tistos_ru_of.sh $foamCaseDir
    cd $foamCaseDir
    
    # 
    # check if mesh is ok
    #
    
    checkMesh > log.checkMesh 2>&1
    if cat "log.checkMesh" | grep -q nonClosedCells || cat "log.checkMesh" | grep -q zeroVolumeCells || cat "log.checkMesh" | grep -q wrongOrientedFaces;then
        exit
    else
        sh sbatch.tistos_ru_of.sh
    fi
    
    #
    # preparing foam case for interpolation
    #

    # calculix can't read .gz, unzip the files

    if ls $foamCaseDir/constant/polyMesh | grep -q gz; then
            echo "unzip files"
            lsOutput=$(ls $foamCaseDir/constant/polyMesh)
            gzFiles=($(echo "$lsOutput" find | grep gz))
            for ((j = 0; j!= ${#gzFiles[@]}; j++)); do
                    gunzip $foamCaseDir/constant/polyMesh/${gzFiles[j]}
            done
    fi
    
    # write values to csv, remove reults, create empty folder last timestep, shift patches to the top of file boundary
    
    patchNameFoam="RU_BLADE"
    timeStep="100" # Muss vor richtigem Lauf geändert werden
    
    searchString=","
    count=`grep -o "$searchString" <<< "$patchNameFoam" | wc -l`
    patchNameFoamCopy=$patchNameFoam
    for ((j = 0; j<=$count; j++)); do
            patchNameFoamArray[j]=${patchNameFoamCopy%%$searchString*}
            patchNameFoamCopy=${patchNameFoamCopy#*$searchString}
    done
    
    if ! ls $foamCaseDir | grep -q ${patchNameFoamArray[0]}_p_$timeStep.csv; then
    echo "writting values to csv, shifting patches to the top of file boundary"
    
    for ((j = 0; j!= ${#patchNameFoamArray[@]}; j++)); do
            patchToCsv p ${patchNameFoamArray[j]} -time $timeStep >> log.patchToCsv 2>&1
            
            # Calculix stops reading patches after a mixing plane. The patches for interpolation have to be shift to the top to avoid this problem
            
            sed -i '/'"${patchNameFoamArray[j]}"'/,/}/w tempFile' constant/polyMesh/boundary
            sed -i '/'"${patchNameFoamArray[j]}"'/,/}/d' constant/polyMesh/boundary
            sed -i -e '/(/{r tempFile' -e ':a;n;ba}' constant/polyMesh/boundary
    done

    # Delete all lines with keyword inGroups, otherwise cgx can`t read the file
    sed -i '/inGroups/d' constant/polyMesh/boundary

    rm -r [0-9]* tempFile
    mkdir $timeStep

    fi
    
    # calculix can't read binary files, convert to ascii

    if cat $foamCaseDir/system/controlDict | grep -q binary; then
            echo "converting binary to ascii"
            sed -e 's%^[ \t]*writeFormat.*%writeFormat\tascii;%g' -i $foamCaseDir/system/controlDict
            foamFormatConvert -constant > log.foamFormatConvert 2>&1
    fi

    
    #
    # run calculix
    #
    cd $_mainDir
    cp sbatch.ipv.ca.sh $calculixCaseDir
    cp sbatch.sim.ca.sh $calculixCaseDir
    
    #
    # start the interpolation
    #
    
    python3 ca.py $foamCaseDir $calculixCaseDir $_state "interpolateValues" > log.interpolating 2>&1
    cd $calculixCaseDir
    sh sbatch.ipv.ca.sh
    
    #
    # write the solver file
    #
    cd $_mainDir
    
    python3 ca.py $foamCaseDir $calculixCaseDir $_state "writeSolverFile"
    
    #
    # start simulation
    #
    cd $calculixCaseDir
    
    sh sbatch.sim.ca.sh
done
    
    
    


