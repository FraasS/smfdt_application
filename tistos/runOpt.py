from oslo_concurrency import lockutils
lockutils.set_defaults(lock_path='/home/stefanf/tmp/')

import sys
sys.path.append("/home/stefanf/sandbox/dtOO/scripts/python")
from pyDtOO import *

import os
import pygmo as pg

from pyDtOO import dtClusteredSingletonState as stateCounter
from pyDtOO import dtPagmo2Binding as tistosRunnerPagmo
from tistosPyBib import *

# Define state counter
stateCounter.PREFIX = 'T1'
stateCounter.CASE = 'tistos_ru_of'
stateCounter.SIMSH = 'simulate.tistos_ru_of.sh'
stateCounter.ADDDATA = ['P', 'dH', 'eta', 'VCav', 'sigma']
stateCounter.ADDDATADEF = ['P', 'dH', 'eta', 'VCav', 'sigma']

tistosRunnerPagmo.PROB = tistosRunner
tistosRunnerPagmo.STATECOUNTER = stateCounter


prob = pg.problem(tistosRunnerPagmo())
pop = pg.population(prob)
pop.push_back([5.102218617525421107e-02, -2.512690957171534456e-03, -5.589888527348368941e-02, 4.969410590983448783e-02,
               3.203488007149858652e-02, 2.983194924995360986e-02, 1.298270270262150916e+00,
               1.179731036098121244e+00, 1.185796315911600951e+00, 4.438544809391175017e-01,
               4.709338161471348094e-01, 5.120795829880013805e-01, 1.274153746092412665e-01,
               -1.250711715565816229e-01, -5.965474914603051404e-02, 4.437710400148663359e-01,
               5.862157143876801646e-01, 7.116450295847175367e-01, 1.310122574698859010e-02, 
               3.243113661650531832e-02, 4.013894985855598341e-02, 5.401324636195827672e-03,
               1.929099197619379633e-02, 6.601490644784414885e-03, 5.429436704648776812e-01,
               4.798514834133842388e-01])


