import sys
import numpy as np
import os
import shutil

sys.path.append('/home/stefanf/sandbox/dtOO/tools/')
from libdtOOPython import *

caseName = sys.argv[1]
state= sys.argv[2] 

xmlName = os.path.join(state + ".xml")

dtXmlParser.init("machine.xml", xmlName)
parser = dtXmlParser.reference()
parser.parse()
bC = baseContainer()
cV = labeledVectorHandlingConstValue()
aF = labeledVectorHandlingAnalyticFunction()
aG = labeledVectorHandlingAnalyticGeometry()
bV = labeledVectorHandlingBoundedVolume()
dC = labeledVectorHandlingDtCase()
dP = labeledVectorHandlingDtPlugin()
parser.createConstValue(cV)
parser.loadStateToConst(state, cV)
parser.destroyAndCreate(bC, cV, aF, aG, bV, dC, dP)

#
# make fem mesh
#

bV.get("ruWithRounding_mechMesh").makeGrid()

#
# make cfd mesh
#
for i in ("_tl", "_n", "_vl"):
    meshFinishedId = os.path.join(caseName + i + "_" + state, "mesh.finished")
    dC.get(caseName + i).runCurrentState()
    f = open(meshFinishedId, "w")
    f.close()
    os.mkdir(os.path.join(caseName + i + "_" + state, "calculix"))
    shutil.copy(state+"_ruWithRounding_mechMesh_2.inp", os.path.join(caseName + i + "_" + state, "calculix"))
    
    





