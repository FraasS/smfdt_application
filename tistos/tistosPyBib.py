import sys
sys.path.append("/home/stefanf/sandbox/dtOO/scripts/python")
sys.path.append("/home/stefanf/sandbox/smfdt/scripts")
from python import *
from pyDtOO import *
import numpy as np
import os

class tistosRunner:
    import sys
    
    OMEGA = 9.42477796
    SIGMAZUL = 80
    DHZUL = -2.0
    
    def __init__(self, case, prefix, stateNumber, coeff):
        self.case_ = case
        self.prefix_ = prefix
        self.stateNumber_ = stateNumber
        self.coeff_ = coeff
        self.state_ = str(self.prefix_)+'_'+str(self.stateNumber_)
        
        self.weights_ = {"tl":1/3, "n":1/3, "vl":1/3}
        
        #
        # Init variables
        #
        
        self.P_ = {"tl":0, "n":0, "vl":0}
        self.dH_ = {"tl":0, "n":0, "vl":0}
        self.eta_ = {"tl":0, "n":0, "vl":0}
        self.Vcav_ = {"tl":0, "n":0, "vl":0}
        self.sigma_ = {"tl":0, "n":0, "vl":0}
        self.isOk_ = False
        
    @staticmethod
    def TransformPre(stateLabel, x, cVstr):
        return x, cVstr
    
    @staticmethod
    def DoF():
        return [
            {'label': 'cV_ru_alpha_1_ex_0.0', 'min': -0.08, 'max': 0.07}, 
            {'label': 'cV_ru_alpha_1_ex_0.5', 'min': -0.08, 'max': 0.07}, 
            {'label': 'cV_ru_alpha_1_ex_1.0', 'min': -0.08, 'max': 0.07}, 
            {'label': 'cV_ru_alpha_2_ex_0.0', 'min': -0.08, 'max': 0.07}, 
            {'label': 'cV_ru_alpha_2_ex_0.5', 'min': -0.08, 'max': 0.07}, 
            {'label': 'cV_ru_alpha_2_ex_1.0', 'min': -0.08, 'max': 0.07}, 
            {'label': 'cV_ru_offsetM_ex_0.0', 'min': 1.0, 'max': 1.5},
            {'label': 'cV_ru_offsetM_ex_0.5', 'min': 1.0, 'max': 1.5}, 
            {'label': 'cV_ru_offsetM_ex_1.0', 'min': 1.0, 'max': 1.5}, 
            {'label': 'cV_ru_ratio_0.0', 'min': 0.4, 'max': 0.6}, 
            {'label': 'cV_ru_ratio_0.5', 'min': 0.4, 'max': 0.6}, 
            {'label': 'cV_ru_ratio_1.0', 'min': 0.4, 'max': 0.6}, 
            {'label': 'cV_ru_offsetPhiR_ex_0.0', 'min': -0.15, 'max': 0.15}, 
            {'label': 'cV_ru_offsetPhiR_ex_0.5', 'min': -0.15, 'max': 0.15}, 
            {'label': 'cV_ru_offsetPhiR_ex_1.0', 'min': -0.15, 'max': 0.15}, 
            {'label': 'cV_ru_bladeLength_0.0', 'min': 0.4, 'max': 0.8}, 
            {'label': 'cV_ru_bladeLength_0.5', 'min': 0.4, 'max': 0.8}, 
            {'label': 'cV_ru_bladeLength_1.0', 'min': 0.4, 'max': 0.8}, 
            {'label': 'cV_ru_t_le_a_0', 'min': 0.005, 'max': 0.06}, 
            {'label': 'cV_ru_t_le_a_1', 'min': 0.005, 'max': 0.06}, 
            {'label': 'cV_ru_t_mid_a_0', 'min': 0.005, 'max': 0.06}, 
            {'label': 'cV_ru_t_mid_a_1', 'min': 0.005, 'max': 0.06}, 
            {'label': 'cV_ru_t_te_a_0', 'min': 0.005, 'max': 0.06}, 
            {'label': 'cV_ru_t_te_a_1', 'min': 0.005, 'max': 0.06}, 
            {'label': 'cV_ru_u_mid_a_0', 'min': 0.4, 'max': 0.6}, 
            {'label': 'cV_ru_u_mid_a_1', 'min': 0.4, 'max': 0.6}
            ]
    
    def ReadResults(self):
        omega = tistosRunner.OMEGA
        try:
            
            for i in ("tl" , "n", "vl"):
                caseDirOf = self.case_+"_"+i+"_"+self.state_
                caseDirCa = os.path.join(caseDirOf, "calculix")
            
                #
                # read openFoam results
                #
                
                Q_ru_dev = dtScalarDeveloping( dtDeveloping(
                    os.path.join(caseDirOf, 'postProcessing/swakExpression_Q_ru_in')
                    ).Read() )
                pIn_ru_dev = dtScalarDeveloping( dtDeveloping(
                    os.path.join(caseDirOf, 'postProcessing/swakExpression_ptot_ru_in')
                    ).Read() )
                pOut_ru_dev = dtScalarDeveloping( dtDeveloping(
                    os.path.join(caseDirOf, 'postProcessing/swakExpression_ptot_ru_out')
                    ).Read() )
                Vcav = dtScalarDeveloping( dtDeveloping(
                    os.path.join(caseDirOf, 'postProcessing/swakExpression_V_CAV')
                    ).Read() )
                F_dev = dtForceDeveloping( dtDeveloping(
                    os.path.join(caseDirOf, 'postProcessing/forces')
                    ).Read({'force.dat' : ':,4:10', 'moment.dat' : ':,4:10', '*.*' : ''}) )
                
                self.P_[i] = F_dev.MomentMeanLast(100)[2] * omega
                self.dH_[i] = (pOut_ru_dev.MeanLast(100) - pIn_ru_dev.MeanLast(100)) / 9.81
                self.eta_[i] = self.P_[i] / (1000. * 9.81 * self.dH_[i] * Q_ru_dev.MeanLast(100) )
                self.Vcav_[i] = Vcav.MeanLast(100)
                
                # check for last iteration
                self.lastIt_ = int( pIn_ru_dev.LastTime() )
                if (self.lastIt_ != 100):
                    raise ValueError('Max number of iterations not reached.')
                    
                #
                # read calculix results
                #
                
                calculixData = calculixPost( calculixPostReadData(
                    caseDirCa, self.state_+"_ruWithRounding_mechMesh_2", "RUBLADE", 1e-6
                    ).Read() )
                self.sigma_[i] = calculixData.GetMaximum()[3][4]
           
            #
            # toggle isOk to True
            #
            self.isOk_ = True 
                
                
        except Exception as e:
            print('Catch exception : ', e)
                
    @staticmethod
    def FailedFitness():
        return [tistosRunner.sys.float_info.max,]
    
    @staticmethod
    def IsFailedFitness(fit):
        if fit[0] == tistosRunner.sys.float_info.max:
            return True
        else:
            return False
        
    def GiveFitness(self):
        sigmaZul = tistosRunner.SIGMAZUL
        dHZul = tistosRunner.DHZUL
        fitness = 0.0
        if self.isOk_:
            for i in ("tl", "n", "vl"):
                switchHead = 1 if i == "n" else 0
                switchMises = 0 if (self.sigma_[i] - sigmaZul) <= 0 else 1
                fitness += self.weights_[i] * (
                        np.abs(1+self.eta_[i]) +
                        (switchHead * (1-(1/np.exp(1*np.abs(self.dH_[i] - dHZul)))) ) +
                        (1-(1/np.exp(0.3*1e3*self.Vcav_[i]))) + 
                        (switchMises * (np.abs(self.sigma_[i] - sigmaZul)/sigmaZul))
                        )
            return [fitness,]
        else:
            return tistosRunner.FailedFitness()



                    
                    

                
                
                
            
            
