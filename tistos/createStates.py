import sys
import numpy as np
import os

sys.path.append('/home/stefanf/sandbox/dtOO/tools/')
from libdtOOPython import *

caseName = sys.argv[1]
state= sys.argv[2] 

xmlName = os.path.join(state + ".xml")

dtXmlParser.init("machine.xml", xmlName)
parser = dtXmlParser.reference()
parser.parse()
bC = baseContainer()
cV = labeledVectorHandlingConstValue()
aF = labeledVectorHandlingAnalyticFunction()
aG = labeledVectorHandlingAnalyticGeometry()
bV = labeledVectorHandlingBoundedVolume()
dC = labeledVectorHandlingDtCase()
dP = labeledVectorHandlingDtPlugin()
parser.createConstValue(cV)
parser.loadStateToConst(state, cV)
parser.destroyAndCreate(bC, cV, aF, aG, bV, dC, dP)
dP.get('ru_adjustDomain').apply()
os.remove(xmlName)
parser.extract(state, cV, xmlName)


    
    





