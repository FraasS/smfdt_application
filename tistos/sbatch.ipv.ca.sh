#!/bin/sh

#
# go to submit directory
#
#cd $SLURM_SUBMIT_DIR

lsOutput=$(ls)
interpolationFile=($(echo "$lsOutput" find | grep fbd))

cgx -bg $interpolationFile > log.ca.interpolating 2>&1

touch interpolation.finished
