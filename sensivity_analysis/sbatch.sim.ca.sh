#!/bin/sh

#
# go to submit directory
#
cd $SLURM_SUBMIT_DIR

lsOutput=$(ls)
solverFile=($(echo "$lsOutput" find | grep final))

export LD_LIBRARY_PATH=/home/st/st_us-042020/st_ac132161/sandbox/calculix/PaStiX/parsec_i8/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=/home/st/st_us-042020/st_ac132161/sandbox/calculix/PaStiX/hwloc_i8/lib:${LD_LIBRARY_PATH}

/home/st/st_us-042020/st_ac132161/sandbox/calculix/bin/ccx ${solverFile:0:-4} > log_ca 2>&1

touch simulation.finished
