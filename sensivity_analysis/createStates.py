import sys
import numpy as np
import os

convertfunc = lambda x: str(x.replace("#", ""))
sample=np.loadtxt(os.path.join(sys.argv[3], "param_values.txt"))
parameterNames=np.genfromtxt(os.path.join(sys.argv[3], "param_values.txt"),converters={0: convertfunc}, encoding=None, delimiter=',', comments='.', dtype='str', skip_footer=int(sys.argv[1]))

sys.path.append('/home/st/st_us-042020/st_ac132161//sandbox/dtOO/tools/')
from libdtOOPython import *

for i in range(int(sys.argv[2] ), int(sys.argv[1])):
  dtXmlParser.init("machine.xml", "machineSave.xml")
  parser = dtXmlParser.reference()
  parser.parse()
  bC = baseContainer()
  cV = labeledVectorHandlingConstValue()
  aF = labeledVectorHandlingAnalyticFunction()
  aG = labeledVectorHandlingAnalyticGeometry()
  bV = labeledVectorHandlingBoundedVolume()
  dC = labeledVectorHandlingDtCase()
  dP = labeledVectorHandlingDtPlugin()
  parser.createConstValue(cV)
  parser.loadStateToConst("E1_12685_eazyE", cV)

  xmlName = os.path.join(sys.argv[4] + "_" + str(i+1) + ".xml")
  stateName = os.path.join(sys.argv[4] + "_" + str(i+1))
  for k in range(np.size(sample[0,:])): 
    cV.get(str.lstrip(parameterNames[k])).setValue(sample[i,k])

  parser.destroyAndCreate(bC, cV, aF, aG, bV, dC, dP)
  parser.extract(stateName, cV, xmlName)


