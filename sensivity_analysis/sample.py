from SALib.sample import morris
import os
import sys
import numpy as np

# Define the model inputs
problem = {
    'num_vars':10,
    'names': ['cV_ruMech_charLMax', 'cV_ruMech_charLMinB' , 'cV_ruMech_charLMinHB',
             'cV_ruMech_distMinB', 'cV_ruMech_distMaxB', 'cV_ruMech_distMinHB', 'cV_ruMech_distMaxHB',
             'cV_ru_t_rounding', 'cV_ru_maxThickness_a_0', 'cV_ru_maxThickness_a_1'],
    'bounds': [[0.03, 0.08],
              [0.004, 0.0125],
              [0.0075, 0.0125],
              [0.02, 0.04],
              [0.08, 0.16],
              [0.02, 0.04],
              [0.08, 0.16],
              [0.1, 0.15],
              [0.37, 0.43],
              [0.06, 0.12]]
}

# Generate samples
param_values = morris.sample(problem,10,4)
fileName = os.path.join(sys.argv[1], "param_values.txt")
header=str(problem['names'])[1:-1] 
np.savetxt(fileName, param_values, header=header.replace("'",""))
