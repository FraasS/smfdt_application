import sys
import numpy as np
import os
import matplotlib.pyplot as plt
import glob
import shutil
import warnings
sys.path.append('/home/st/st_us-042020/st_ac132161/sandbox/smt00/scripts/')
from python import *

#
# functions
#

def GetArrayFromString(string):
  numberOfArrayElements = 1
  for i in string:
    if i == ",":
      numberOfArrayElements += 1

  counter = 0
  arrayFromString = [""] * numberOfArrayElements
  for i in string:
    if i == ",":
      counter += 1
    else:
      arrayFromString[counter] += i
  return arrayFromString

def GetVariableName(int):
  if int == 1:
    return "Mises"
  elif int == 2:
    return "dz"
  elif int == 3:
    return "dy"
  else:
    return "dx"

def WriteData(path, name, valueList, averaged = False, append = False):
  if averaged:
    f = open(os.path.join(path, name + ".csv"), "w")
    f.write("# dx, dy, dz, Mises \n")
    f.write(os.path.join(str(valueList[0]) + ", " + str(valueList[1]) + ", " + str(valueList[2]) + ", " + str(valueList[3])))
    f.close()
  else:    
    if append:
      f = open(os.path.join(path + ".txt"), "a+")
      f.write("\n")
      f.write(name)
      f.write("\n# Line1: x, y, z, dx; # Line2: x , y, z, dy; # Line3: x , y, z, dz; # Line4: x , y, z, Mises")
      for i in range(np.size(valueList[:,0])):
        f.write("\n")
        f.write(os.path.join(str(valueList[i,0]) + ", " + str(valueList[i,1]) + ", " + str(valueList[i,2]) + ", " + str(valueList[i,3])))
        f.write("\n")
      f.close()
    else:
      f = open(os.path.join(path, name + ".csv"), "w")
      f.write("# Line1: x, y, z, dx\n# Line2: x , y, z, dy\n# Line3: x , y, z, dz\n# Line4: x , y, z, Mises")
      for i in range(len(valueList)):
        f.write("\n")
        f.write(os.path.join(str(valueList[i][1]) + ", " + str(valueList[i][2]) + ", " + str(valueList[i][3]) + ", " + str(valueList[i][4])))
      f.close()


#
# preProcessing
#

if sys.argv[len(sys.argv)-1] == "interpolateValues":
  patchNamesFoam = GetArrayFromString(sys.argv[3])
  patchNamesCalculix = GetArrayFromString(sys.argv[5])

  wssap = writeShearStressAndPressure(sys.argv[1], sys.argv[2], patchNamesFoam, int(sys.argv[4]))
  wssap.WritePressure()
  if (sys.argv[9] != "none"):
    wssap.WriteShearStress()

  foamCase = os.path.join("..",sys.argv[1])

  pathCalculixCase = os.path.join(sys.argv[7], "valueInterpolation.fbd")

  f = open(pathCalculixCase, "w")
  f.write("read ")
  f.write(foamCase)
  f.write(" foam")
  f.write("\nread ")
  f.write(sys.argv[6])
  f.write(" add")
  f.write("\nseto master\n")
  for i in range(np.size(patchNamesFoam)):
    f.write("seta master f ")
    f.write(patchNamesFoam[i])
    f.write("\n")
  f.write("comp master do\n")
  for i in range(np.size(patchNamesCalculix)):
    f.write("seta pressure n ")
    f.write(patchNamesCalculix[i])
    f.write("\n")
  f.write("comp pressure do\n")
  if (sys.argv[9] != "none"):
    for i in range(np.size(patchNamesCalculix)):
      f.write("seta shear n ")
      f.write(patchNamesCalculix[i])
      f.write("\n")
      f.write("comp shear do\n")
  f.write("map pressure master surf ds1\nsend pressure abq pres ds1 e1")
  if (sys.argv[9] != "none"):
    f.write("\nmap shear master surf ds2")
    f.write("\nsend shear abq pres ds2 e1")
    f.write("\nsend shear abq pres ds2 e2")
    f.write("\nsend shear abq pres ds2 e3")
  f.write("\nsend ")
  f.write(sys.argv[8])
  f.write(" abq")
  f.write("\n\nquit")
  f.close()

elif sys.argv[len(sys.argv)-1] == "writeSolverFile":
  #Get necessary arrays from string
  boundaryName = GetArrayFromString(sys.argv[4])
  setName = GetArrayFromString(sys.argv[5])
  setNameForce = GetArrayFromString(sys.argv[7])
  rotationAxis = GetArrayFromString(sys.argv[9])
  
  #Get solver name consisting of two words
  solver = sys.argv[13].replace("_", " ")

  if sys.argv[6] == "False":
    force = False
  else:
    force = True

  #write the solver file
  wsf = writeSolverFile(sys.argv[1], sys.argv[2], sys.argv[3], boundaryName, setName, force, setNameForce, \
                        float(sys.argv[8]), rotationAxis, sys.argv[10], sys.argv[11], sys.argv[12], solver)

  wsf.WriteSolverFileGmsh()



elif sys.argv[len(sys.argv)-1] == "writeSolverInterpolationFile":
  wsf = writeSolverFile(sys.argv[1], sys.argv[2])
  wsf.WriteSolverInterpolateFile()

#
# postProcessing
#
  
elif sys.argv[len(sys.argv)-1] == "postProcessingWriteData":
  patchNameCalculix = GetArrayFromString(sys.argv[3])
  setName = GetArrayFromString(sys.argv[5])

  cP = calculixPost(calculixPostReadData(sys.argv[1], sys.argv[2], patchNameCalculix).Read())

  #write the maximum values
  maximum = cP.GetMaximum()
  WriteData(sys.argv[1], "maximum", maximum)

  #write the area averaged values
  averagedValues = cP.GetAverageValues(sys.argv[4], setName)
  WriteData(sys.argv[1], "averagedValues", averagedValues, averaged = True)


elif sys.argv[len(sys.argv)-1] == "postProcessing":
  folderList = (glob.glob('*case'))
  path = os.path.join(sys.argv[1], "results_ca.txt")
  stateName = os.path.join(sys.argv[2] + "_")

  # read the maximum and averaged values
  for i in range(np.size(folderList)):
    valuesMaximum = np.full([4, 4], np.nan)
    valuesAveraged = np.full(4, np.nan)
    try:
      valuesMaximum = np.genfromtxt(os.path.join(folderList[i], "maximum.csv"), delimiter = ",", comments = "#")
      valuesAveraged = np.genfromtxt(os.path.join(folderList[i], "averagedValues.csv"), delimiter = ",", comments = "#")
    except IOError:
      warningMessage = \
        os.path.join("No values for maximum ore averaged found for case " + folderList[i].replace('_ruWithRounding_mechMesh_2_case', '') + ": Deviation will not be calculated")
      warnings.warn(warningMessage)
      pass
    
    # get radius and phi 
    phi = np.arctan2(valuesMaximum[3,1], valuesMaximum[3,0])
    radius = np.linalg.norm([valuesMaximum[3,0], valuesMaximum[3,1]])

    # write results
    folderList[i] = folderList[i].replace(os.path.join("_" + sys.argv[3] + "_2_case"), "")
    if i == 0:
      f = open(path, "w")
    else:
      f = open(path, "a+")
      f.write("\n")
    f.write(str(folderList[i]))
    f.write(", ")
    f.write(str(valuesMaximum[3,3]))
    f.write(", ")
    f.write(str(radius))
    f.write(", ")
    f.write(str(phi))
    f.write(", ")
    f.write(str(valuesAveraged[3]))
    f.close()

  # sort
  resultData = np.genfromtxt(path, delimiter = ',', dtype = str)
  sampleNames = np.zeros(np.size(resultData[:,0]))
  for i in range(np.size(resultData[:,0])):
    sampleNames[i] = int(resultData[i,0].replace(stateName, ''))
  indexList = sorted(range(len(sampleNames)), key=lambda k: sampleNames[k])
  resultDataCopy = resultData.copy()
  for i in range(np.size(resultData[:,0])):
      resultData[i] = resultDataCopy[indexList[i]]
  f = open(path, "w")
  f.write("# Case, Mises, Radius, Phi, averadedMises")
  f.write("\n")
  np.savetxt(f, resultData, fmt="%s, %s, %s, %s, %s", comments='', delimiter=',')
  f.close()

  
else:
  warnings.warn("Don't now what to do, check flag in initialisation Allrun")

  

