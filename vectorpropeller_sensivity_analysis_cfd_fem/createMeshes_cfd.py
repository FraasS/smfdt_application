import sys
import numpy as np
import os

convertfunc = lambda x: str(x.replace("#", ""))
sample=np.loadtxt(os.path.join(sys.argv[3], "param_values.txt"))
parameterNames=np.genfromtxt(os.path.join(sys.argv[3], "param_values.txt"),converters={0: convertfunc}, encoding=None, delimiter=',', comments='.', dtype='str', skip_footer=int(sys.argv[2]))

sys.path.append('/home/st/st_us-042020/st_ac132161/sandbox/dtOO/tools/')
from libdtOOPython import *
#logMe.initLog('build.log')

xmlName = os.path.join(sys.argv[4] + "_" + sys.argv[1] + ".xml")
stateName = os.path.join(sys.argv[4] + "_" + sys.argv[1])
meshFinishedId = os.path.join(sys.argv[5] + "_" + stateName, "mesh.finished")

dtXmlParser.init("machine.xml", xmlName)
parser = dtXmlParser.reference()
parser.parse()
bC = baseContainer()
cV = labeledVectorHandlingConstValue()
aF = labeledVectorHandlingAnalyticFunction()
aG = labeledVectorHandlingAnalyticGeometry()
bV = labeledVectorHandlingBoundedVolume()
dC = labeledVectorHandlingDtCase()
dP = labeledVectorHandlingDtPlugin()
parser.createConstValue(cV)
parser.loadStateToConst(stateName, cV)
parser.destroyAndCreate(bC, cV, aF, aG, bV, dC, dP)
dC.get(str(sys.argv[5])).runCurrentState()
f = open(meshFinishedId, "w")
f.close()




