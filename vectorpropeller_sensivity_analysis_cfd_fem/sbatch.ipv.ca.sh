#!/bin/sh

#
# go to submit directory
#
cd $SLURM_SUBMIT_DIR

lsOutput=$(ls)
interpolationFile=($(echo "$lsOutput" find | grep fbd))

/home/st/st_us-042020/st_ac132161/sandbox/calculix/bin/cgx -bg $interpolationFile > log.ca.interpolating 2>&1

touch interpolation.finished
