from SALib.sample import morris
import os
import sys
import numpy as np

# Define the model inputs
problem = {
    'num_vars':4,
    'names': ['cV_ru_t_te_a_0', 'cV_ru_t_te_a_1', 'cV_ru_t_mid_a_0', 'cV_ru_t_mid_a_1'],
    'bounds': [[0.0027459048797828225, 0.003],
              [0.0021338069751956983, 0.0023],
              [0.0071069770386736454, 0.009],
              [0.0032975785552733154, 0.005]]
}

# Generate samples
param_values = morris.sample(problem,10,4)
fileName = os.path.join(sys.argv[1], "param_values.txt")
header=str(problem['names'])[1:-1] 
np.savetxt(fileName, param_values, header=header.replace("'",""))
