#!/bin/sh

#
# go to submit directory
#
cd $SLURM_SUBMIT_DIR

module load cae/openfoam/v2012
source $FOAM_INIT

export FOAM_SIGFPE=false

if find | grep -q $5"_"$4"_"$1; then 
	if ls $5"_"$4"_"$1 | grep -q mesh.finished; then
		echo "mesh for case" $4"_"$1 "allready exits, skipping mesh generation"
		exit
	else
		python3 createMeshes_cfd.py $1 $2 $3 $4 $5 > log.mesh_$4_$1 2>&1
	fi
else
	python3 createMeshes_cfd.py $1 $2 $3 $4 $5 > log.mesh_$4_$1 2>&1
fi
