#!/bin/sh

#
# load OpenFOAM
#
module load cae/openfoam/v2012
source $FOAM_INIT

#
# go to submit directory
#
cd $SLURM_SUBMIT_DIR

rm -rf processor*

sed -i "s/numberOfSubdomains.*;/numberOfSubdomains $SLURM_NPROCS;/" system/decomposeParDict
decomposePar > log_dp 2>&1

sed -e 's%^[ \t]*writeInterval.*%writeInterval 100;%g' -i system/controlDict
sed -e 's%^[ \t]*endTime.*%endTime 100;%g' -i system/controlDict
sed -e 's%turbulence.*on%turbulence off%g' -i constant/turbulenceProperties
sed -e 's%cellL%faceL%g' -i system/fvSchemes
mpirun --bind-to core --map-by core -report-bindings simpleFoam -parallel > log_of 2>&1

sed -e 's%^[ \t]*writeInterval.*%writeInterval 1000;%g' -i system/controlDict
sed -e 's%^[ \t]*endTime.*%endTime 1000;%g' -i system/controlDict
sed -e 's%turbulence.*off%turbulence on%g' -i constant/turbulenceProperties
sed -e 's%faceL%cellL%g' -i system/fvSchemes
mpirun --bind-to core --map-by core -report-bindings simpleFoam -parallel >> log_of 2>&1

reconstructPar

rm -rf processor*

touch simulation.finished
