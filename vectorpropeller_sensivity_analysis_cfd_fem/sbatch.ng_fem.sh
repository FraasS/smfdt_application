#!/bin/sh

#
# go to submit directory
#
cd $SLURM_SUBMIT_DIR

module load cae/openfoam/v2012
source $FOAM_INIT

#export FOAM_SIGFPE=false

if ls $pathToMeshGeneration | grep -q $4"_"$1"_"*.*2.inp; then

	echo "mesh for case" $4"_"$1 "allready exits, skipping mesh generation"
	exit
else
	python3 createMeshes.py $1 $2 $3 $4 $5  > log.mesh_fem_$4_$1 2>&1
fi
