import sys
import numpy as np
import os

convertfunc = lambda x: str(x.replace("#", ""))
sample=np.loadtxt(os.path.join(sys.argv[3], "param_values.txt"))
parameterNames=np.genfromtxt(os.path.join(sys.argv[3], "param_values.txt"),converters={0: convertfunc}, encoding=None, delimiter=',', comments='.', dtype='str', skip_footer=int(sys.argv[1]))

sys.path.append('/home/st/st_us-042020/st_ac132161/sandbox/dtOO/tools/')
from libdtOOPython import *
logMe.initLog('build.log')

for i in range(int(sys.argv[2] ), int(sys.argv[1])):
  dtXmlParser.init("machine.xml", "I1_8262.xml")
  parser = dtXmlParser.reference()
  parser.parse()
  bC = baseContainer()
  cV = labeledVectorHandlingConstValue()
  aF = labeledVectorHandlingAnalyticFunction()
  aG = labeledVectorHandlingAnalyticGeometry()
  bV = labeledVectorHandlingBoundedVolume()
  dC = labeledVectorHandlingDtCase()
  dP = labeledVectorHandlingDtPlugin()
  parser.createConstValue(cV)
  parser.loadStateToConst("I1_8262", cV)

  xmlName = os.path.join(sys.argv[4] + "_" + str(i+1) + ".xml")
  stateName = os.path.join(sys.argv[4] + "_" + str(i+1))
  for k in range(np.size(sample[0,:])): 
    cV.get(str.lstrip(parameterNames[k])).setValue(sample[i,k])
  cV.get("cV_ruMech_charLMax").setValue(0.0135)
  cV.get("cV_ruMech_charLMinB").setValue(0.00135)
  cV.get("cV_ruMech_charLMinHB").setValue(0.00135)
  cV.get("cV_ru_t_rounding").setValue(0.01)
  cV.get("cV_ruMech_distMinB").setValue(0.0045)
  cV.get("cV_ruMech_distMaxB").setValue(0.035)
  cV.get("cV_ruMech_distMinHB").setValue(0.0045)
  cV.get("cV_ruMech_distMaxHB").setValue(0.035)
  cV.get("cV_ru_nBoundaryLayersAddExt").setValue(8)
  parser.destroyAndCreate(bC, cV, aF, aG, bV, dC, dP)
  dP.get("ru_adjustDomain").apply()
  dP.get("ru_smoothGrading").apply()
  parser.extract(stateName, cV, xmlName)



